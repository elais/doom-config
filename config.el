;;; ~/.doom.d/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here
(setq-default evil-shift-width 2
              tab-width 2
              c-basic-offset 2)

(setq user-email-address "elais@protonmail.com"
      user-full-name "Elais Player")

(setq display-line-numbers-type nil)

(def-package! tao-theme
  :init
  (setq tao-theme-use-boxes nil
        tao-theme-sepia-depth 5
        tao-theme-sepia-saturation 1.02)
  :config
  (load-theme 'tao-yang t))
