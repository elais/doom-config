;; -*- no-byte-compile: t; -*-
;;; lang/gerbil/packages.el

(package! gambit :recipe (:host file :path (concat (getenv "GAMBIT_HOME") "/share/emacs/site-lisp/gambit.el")))
(package! gerbil :recipe (:host file :path (concat (getenv "GERBIL_HOME") "/etc/gerbil.el")))
(package! treadmill :recipe (:host github :repo "thunknyc/emacs-treadmill"))
